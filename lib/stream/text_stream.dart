import 'package:flutter/material.dart';

class TextStream {
  // Stream textStream;

  Stream<String> getTexts() async* {
    final List<String> texts = [
      "LOVE",
      "HATE",
      "KILL",
      "SAD",
      "HELLO"
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % 5;
      return texts[index].toLowerCase();
    });
  }
}