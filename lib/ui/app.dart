import 'package:flutter/material.dart';
import '../stream/text_stream.dart';

// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return HomePageStream();
//   }
//
// }

class App extends StatefulWidget {


  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }

}

class HomePageState extends State<StatefulWidget> {
  String text ='';
  TextStream textStream = TextStream();
  final List<String> texts =  [];


  @override
  Widget build(BuildContext context) {
    const title = 'text list';
    return MaterialApp(
      title: title,

      home: Scaffold(
        appBar: AppBar(title:const Text(title)),
        body: ListView.builder(
            itemCount: texts.length,
            itemBuilder: (context, index){
              return ListTile(
                title: Text(texts[index]),
              );
            }),
        floatingActionButton: FloatingActionButton(
          child: Text('change'),
          onPressed: () {
            changeText();
          },
        ),
      ),
    );
  }

  changeText() async {
    textStream.getTexts().listen((eventText) {
      setState(() {
        print(eventText);
        text = eventText;
        texts.add(text);

      });
    });
  }


}